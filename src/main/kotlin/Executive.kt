import java.time.LocalDate

class Executive(name: String, firstname: String, birthdate: LocalDate, var grade: Grade): Employee(name = name, firstname = firstname, birthdate = birthdate) {
    /**
     * Initialisation du salaire lors de la construction d'un objet cadre
     */
    init {
        this.salary = this.getAge().toDouble() * 100 + grade.money
    }

    /**
     * Retourne une chaîne de caractère affichant tous les attributs du cadre
     */
    override fun toString(): String {
        return super.toString() + ", grade : $grade"
    }

    /**
     * Fixe au grade la valeur newGrade et change le salaire du cadre en conséquence
     */
    @JvmName("setGrade1")
    fun setGrade(newGrade: Grade){
        this.salary += newGrade.money - this.grade.money
        this.grade = newGrade
    }
}