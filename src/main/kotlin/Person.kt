import java.time.LocalDate

open class Person(protected var name: String, protected val firstname: String, protected val birthdate: LocalDate) {

    /**
    Retourne une chaîne de caractère affichant tous les attributs de la personne
     */
    override fun toString(): String {
        return "name : $name, firstname : $firstname, birthdate : $birthdate "
    }

    /**
    Retourne le nom de la personne
     */
    @JvmName("getName1")
    fun getName(): String{
        return name
    }

    /**
    Retourne le prénom de la personne
     */
    @JvmName("getFirstname1")
    fun getFirstname(): String{
        return firstname
    }

    /**
    Retourne la date de naissance de la personne
     */
    @JvmName("getBirthdate1")
    fun getBirthdate(): LocalDate{
        return birthdate
    }

    /**
    Fixe au nom la valeur newName
     */
    @JvmName("setName1")
    fun setName(newName: String){
        this.name = newName
    }

    /**
     * Retourne l'age de la personne en année
     */
    fun getAge(): Int {
        var diffY = LocalDate.now().year - birthdate.year
        if(LocalDate.now().month == birthdate.month) {
            if (LocalDate.now().dayOfMonth < birthdate.dayOfMonth){
                diffY--
            }
        } else if(LocalDate.now().month < birthdate.month){
            diffY--
        }
        return diffY
    }
}