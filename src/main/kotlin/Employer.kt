import java.awt.geom.QuadCurve2D

class Employer(var companyName: String, var turnover: Double, var staff: MutableSet<Employee> = mutableSetOf()){
    /**
     * Retourne une chaîne de caractère affichant tous les attributs de l'empoyeur
     */
    override fun toString(): String {
        return "Company name : $companyName, turnover : $turnover, Nb of employee : ${staff.size}"
    }

    /**
     * Ajoute une nouvelle personne dans la liste d'employé de l'entreprise, en tant que
     * cadre si un grade est passé en paramètre ou en tant qu'employé sinon
     */
    fun enroll(person: Person, grade: Grade?){
        val newEmp = when (grade) {
            null -> Employee(person.getName(), person.getFirstname(), person.getBirthdate())
            is Grade -> Executive(person.getName(), person.getFirstname(), person.getBirthdate(), grade)
        }
        staff.add(newEmp)
    }

    /**
     * Retire un employé de la liste d'employé de l'entreprise
     */
    fun dismiss(name: String, firstname: String){
        staff.removeIf { it.getName() == name && it.getFirstname() == firstname }
    }

    /**
     * Augmente le salaire d'un employé ou d'un cadre de l'entreprise, le salaire d'un employé ne peut pas être inférieur
     * au salaire minimal (age * 100)
     */
    fun increase(name: String, firstname: String, amount: Double){
        if(staff.single { it.getName() == name && it.getFirstname() == firstname }.salary + amount < staff.single { it.getName() == name && it.getFirstname() == firstname }.getAge() * 100) {
            throw NumberFormatException()
        }
        staff.single { it.getName() == name && it.getFirstname() == firstname }.salary += amount
    }

    /**
     * Change le nom d'un employé de l'entreprise
     */
    fun changeName(name: String, firstname: String, newName: String){
        staff.single { it.getName() == name && it.getFirstname() == firstname }.setName(newName)
    }

    /**
     * Change le grade d'un employé ou d'un cadre de l'entreprise
     */
    fun upgrade(name: String, firstname: String, newGrade: Grade?){
        var upPerson = staff.single { it.getName() == name && it.getFirstname() == firstname }
        when (newGrade) {
            is Grade -> {
                if (upPerson is Executive) {
                    upPerson.setGrade(newGrade)
                } else {
                    dismiss(upPerson.getName(), upPerson.getFirstname())
                    enroll(Executive(upPerson.getName(), upPerson.getFirstname(), upPerson.getBirthdate(), newGrade), newGrade)
                }
            }
            else -> {
                if(upPerson is Executive) {
                    dismiss(upPerson.getName(), upPerson.getFirstname())
                    enroll(
                        Employee(
                            upPerson.getName(),
                            upPerson.getFirstname(),
                            upPerson.getBirthdate()),null)
                }
            }
        }
    }

    /**
     * Affiche le nom de tous les employés et cadres de l'entreprise
     */
    fun displayName(){
        println("Liste de tous les noms des employés de l'entreprise")
        staff.forEach { println(it.getName()) }
        println()
    }

    /**
     * Affiche chaque cadre de l'entreprise
     */
    fun displayExecutive(){
        println("Liste de tous les cadres de l'entreprise")
        staff.forEach {
            when (it) {
                is Executive -> println(it.toString())
            }
        }
        println()
    }

    /**
     * Affiche le nom de chaque employé et cadre avec le salaire leur correspondant
     */
    fun displaySalary(){
        println("Salaire de chaque personne de l'entreprise")
        staff.forEach { println("Nom : " + it.getName() + ", Salaire : " + it.salary) }
        println()
    }
}