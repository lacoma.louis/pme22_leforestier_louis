import java.time.LocalDate

open class Employee(name: String, firstname: String, birthdate: LocalDate): Person(name = name, firstname = firstname, birthdate = birthdate) {
    var salary: Double

    /**
    Initialisation d'un employé, il ne peut pas avoir moins de 16 ans et son salaire doit être équivalent à son age * 100
     */
    init {
        if(this.getAge() < 16) throw ExceptionInInitializerError("The person has to be at least 16 years old to work")
        this.salary = this.getAge().toDouble() * 100
    }

    /**
    Retourne une chaîne de caractère affichant tous les attributs de l'employé
     */
    override fun toString(): String {
        return super.toString() + ", salary: $salary"
    }
}