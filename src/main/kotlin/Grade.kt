enum class Grade(val money: Double) {
    A(300.0), B(200.0), C(100.0)
}