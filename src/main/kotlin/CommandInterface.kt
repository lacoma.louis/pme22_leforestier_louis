import java.lang.Error
import java.lang.Exception
import java.lang.NullPointerException
import java.time.DateTimeException
import java.time.LocalDate

class CommandInterface(employer: Employer) {
    var company = employer
    private var personList: MutableList<Person> = mutableListOf<Person>()

    fun mainMenu(){
        println("Welcome to the main menu, please choose a number to perform an action")
        println("1. Person menu")
        println("2. Company menu")
        try {
            var choice: Int = readln().toInt()
            println()
            when(choice) {
                1 -> personMenu()
                2 -> companyMenu()
                else -> mainMenu()
            }
        } catch(e: NumberFormatException) {
            println("Please enter a number")
            mainMenu()
        }
    }

    private fun personMenu(){
        println("Welcome to the person menu, please choose a number to perform an action")
        println("1. Create a Person")
        println("2. Display a Person")
        println("3. Display all existing persons")
        println("4. Return to the main menu")
        try {
            var choice: Int = readln().toInt()
            println()
            when(choice) {
                1 -> createPerson()
                2 -> displayPerson()
                3 -> {
                    if(personList.size > 0){
                        println("A list of all the persons: ")
                        personList.forEach { println(it.toString()) }
                    } else println("There's no person to show")
                    println()
                    personMenu()
                }
                4 -> mainMenu()
                else -> personMenu()
            }
        } catch(e: NumberFormatException) {
            println("Please enter a number")
            personMenu()
        }
    }

    private fun createPerson(){
        println("Please enter a name to assign to the person (String):")
        var personName: String = readln()
        println("Please enter a firstname to assign to the person (String):")
        var personFirstName: String = readln()
        println("Please enter the year of the birthdate to assign to the person (Int):")
        try {
            var year: Int = readln().toInt()
            println("Please enter the month of the birthdate to assign to the person (Int):")
            try {
                var month: Int = readln().toInt()
                println("Please enter the day of the birthdate to assign to the person (Int):")
                 try {
                     var day: Int = readln().toInt()
                     try {
                         var person = Person(personName, personFirstName, LocalDate.of(year, month, day))
                         personList.add(person)
                         println("Operation validated")
                         println()
                         personMenu()
                     } catch (e: DateTimeException) {
                         println("Please enter a valid birthdate")
                         personMenu()
                     }
                 } catch (e: NumberFormatException) {
                     println("Please enter a number for the dates")
                     personMenu()
                 }
            } catch (e: NumberFormatException) {
                println("Please enter a number for the dates")
                personMenu()
            }
        } catch (e: NumberFormatException) {
            println("Please enter a number for the dates")
            personMenu()
        }
    }

    private fun displayPerson(){
        if(personList.size < 1){
            println("There's no person to show")
            println()
            personMenu()
        }
        println("Please enter the name of the person you want to see (String):")
        var personName: String = readln()
        when(personList.filter { it.getName() == personName}.size){
            1 -> println(personList.filter { it.getName() == personName})
            else -> {
                println("Please enter the firstname of the person you want to see (String):")
                var personFirstName: String = readln()
                try {
                    println(personList.first { it.getName() == personName && it.getFirstname() == personFirstName }
                        .toString())
                } catch (e: NoSuchElementException) {
                    println("This person doesn't exist, please try again")
                }
            }
        }
        println()
        personMenu()
    }

    private fun companyMenu(){
        println("Welcome to the menu of ${company.companyName}, please choose a number to perform an action")
        if(personList.size > 0) {
            println("1. Enroll a Person")
        }
        if(company.staff.size > 0){
            println("2. Dismiss a Person")
            println("3. Increase the salary of an employee")
            println("4. Change the name of an employee")
            println("5. Upgrade the rank of an employee")
            println("6. Display the name of each employee of the company")
            println("7. Display all the executives in the company")
            println("8. Display the name and salary of each employee in the company")
        }
        println("9. Display the company name and turnover")
        println("10. Return to the main menu")
        try {
            var choice: Int = readln().toInt()
            println()
            when(choice){
                1 -> enrollMenu()
                2 -> dismissMenu()
                3 -> increaseSalMenu()
                4 -> changeNameMenu()
                5 -> upgradeMenu()
                6 -> {
                    if(company.staff.size == 0){
                        println("There's no employee to display")
                    } else {
                        company.displayName()
                    }
                    companyMenu()
                }
                7 -> {
                    if(company.staff.size == 0){
                        println("There's no executive to display")
                    } else {
                        company.displayExecutive()
                    }
                    companyMenu()
                }
                8 -> {
                    if(company.staff.size == 0){
                        println("There's no salary to display")
                    } else {
                        company.displaySalary()
                    }
                    companyMenu()
                }
                9 -> {
                    println(company.toString())
                    println()
                    companyMenu()
                }
                10 -> {
                    println()
                    mainMenu()
                }
                else -> {
                    println()
                    companyMenu()
                }
            }
        } catch (e: NumberFormatException) {
            println("Please enter a number between 1 and 10")
            println()
            companyMenu()
        }
    }

    private fun enrollMenu(){
        if(personList.size == 0) {
            println("There's noone to enroll, create a person first")
            println()
            mainMenu()
        }
        println("Please enter the name of the person to enroll (String):")
        var personName: String = readln()
        when(personList.filter { it.getName() == personName}.size) {
            0 -> {
                println("This person doesn't exist")
                println()
                companyMenu()
            }

            1 -> {
                println("Please enter the grade if you want an executive (A,B,C) or nothing if you want an employee")
                var eGrade: Grade? = when (readln().uppercase()) {
                    "A" -> Grade.A
                    "B" -> Grade.B
                    "C" -> Grade.C
                    else -> null
                }
                try {
                    company.enroll(personList.first { it.getName() == personName }, eGrade)
                    println("Operation validated")
                } catch (e: ExceptionInInitializerError) {
                    println("This person cannot work because of his age, please behave like a human and enroll eligible employee")
                }
            }
            else -> {
                println("Please enter the firstname of the person to enroll (String):")
                var personFirstName: String = readln()
                println("Please enter the grade if you want an executive (A,B,C) or nothing if you want an employee")
                var eGrade: Grade? = when (readln().uppercase()) {
                    "A" -> Grade.A
                    "B" -> Grade.B
                    "C" -> Grade.C
                    else -> null
                }
                try {
                    company.enroll(
                        personList.first { it.getName() == personName && it.getFirstname() == personFirstName },
                        eGrade
                    )
                    println("Operation validated")
                } catch (e: ExceptionInInitializerError) {
                    println("This person cannot work because of his age, please enroll eligible employee (16 year or +)")
                }
            }
        }
        println()
        companyMenu()
    }

    private fun dismissMenu(){
        if(company.staff.size < 1) {
            println("Not a valid number for now")
            println()
            companyMenu()
        }
        println("Please enter the name of the employee to dismiss (String):")
        var personName: String = readln()
        when(personList.filter { it.getName() == personName}.size){
            0 -> {
                println("This employee doesn't exist")
                println()
                companyMenu()
            }
            1 -> {
                company.dismiss(personName, personList.first { it.getName() == personName}.getFirstname())
                println("Operation validated")
            }
            else -> {
                println("Please enter the firstname of the employee to dismiss (String):")
                var personFirstName: String = readln()
                try {
                    company.dismiss(personName, personFirstName)
                    println("Operation validated")
                } catch (e: ExceptionInInitializerError) {
                    println("This person cannot work because of his age, please behave like a human and enroll eligible employee")
                }
            }
        }
        println()
        companyMenu()
    }

    private fun increaseSalMenu(){
        if(company.staff.size < 1) {
            println("Not a valid number for now")
            println()
            companyMenu()
        }
        println("Please enter the name of the employee to increase (String):")
        var personName: String = readln()
        when(personList.filter { it.getName() == personName}.size){
            0 -> {
                println("This employee doesn't exist")
                println()
                companyMenu()
            }
            1 -> {
                println("Please enter the amount you want the employee to be increased (Double):")
                var amount: Double = readln().toDouble()
                try {
                    company.increase(personName, personList.single { it.getName() == personName}.getFirstname(), amount)
                } catch (e: NumberFormatException){
                    println("Please don't be too harsh on the employee, they want to live too")
                    companyMenu()
                }
            }
            else -> {
                println("Please enter the firstname of the employee to increase (String):")
                var personFirstName: String = readln()
                println("Please enter the amount you want the employee to be increased (Double):")
                var amount: Double = readln().toDouble()
                try {
                    company.increase(personName, personFirstName, amount)
                } catch (e: NumberFormatException){
                    println("Please don't be too harsh on the employee, they want to live too")
                    companyMenu()
                }
            }
        }
        println("Operation validated")
        println()
        companyMenu()
    }

    private fun changeNameMenu() {
        if (company.staff.size < 1) {
            println("Not a valid number for now")
            println()
            companyMenu()
        }
        println("Please enter the name of the employee to increase (String):")
        var personName: String = readln()
        when (personList.filter { it.getName() == personName }.size) {
            0 -> {
                println("This employee doesn't exist")
                println()
                companyMenu()
            }
            1 -> {
                println("Please enter the new name you want the employee to have (String):")
                var newName: String = readln()
                try {
                    company.changeName(personName, personList.first { it.getName() == personName }.getFirstname(), newName)
                } catch(e: NoSuchElementException) {
                    println("This employee doesn't exist")
                    println()
                    companyMenu()
                }
            }

            else -> {
                println("Please enter the firstname of the employee to increase (String):")
                var personFirstName: String = readln()
                println("Please enter the new name you want the employee to have (String):")
                var newName: String = readln()
                try {
                    company.changeName(personName, personFirstName, newName)
                } catch (e: NoSuchElementException) {
                    println("This employee doesn't exist")
                    println()
                    companyMenu()
                }
            }

        }
        println("Operation validated")
        println()
        companyMenu()
    }

    private fun upgradeMenu() {
        if (company.staff.size < 1) {
            println("Not a valid number for now")
            println()
            companyMenu()
        }
        println("Please enter the name of the employee to increase (String):")
        var personName: String = readln()
        when (personList.filter { it.getName() == personName }.size) {
            1 -> {
                println("Please enter the grade you want the employee to have (A,B,C) or nothing if you want the executive to lose his grade")
                var eGrade: Grade? = when (readln().uppercase()) {
                    "A" -> Grade.A
                    "B" -> Grade.B
                    "C" -> Grade.C
                    else -> null
                }
                try {
                    company.upgrade(personName, personList.first { it.getName() == personName }.getFirstname(), eGrade)
                } catch (e: NoSuchElementException) {
                    println("There's no such employee")
                }
            }

            else -> {
                println("Please enter the firstname of the employee to increase (String):")
                var personFirstName: String = readln()
                println("Please enter the grade you want the employee to have (A,B,C) or nothing if you want the executive to lose his grade")
                var eGrade: Grade? = when (readln().uppercase()) {
                    "A" -> Grade.A
                    "B" -> Grade.B
                    "C" -> Grade.C
                    else -> null
                }
                try {
                    company.upgrade(personName, personFirstName, eGrade)
                    println("Operation validated")
                } catch (e: NoSuchElementException) {
                    println("There's no such employee")
                }
            }
        }
        println()
        companyMenu()
    }
}