import com.sun.jdi.DoubleValue
import java.time.LocalDate

fun main() {
    var lo = true
    println("Hello Mr. Carno")
    println("This interface works by creating a company, then creating persons, then enrolling those person in the company")
    println()
    println("Enter the name of the company you want to create (String):")
    var comp_name: String = readln()
    while (lo){
        println("Enter the turnover the company has (Double):")
        try {
            var comp_CA: Double = readln().toDouble()
            var employer = Employer(comp_name, comp_CA)
            var commandInterface = CommandInterface(employer)
            lo = false
            println()

            commandInterface.mainMenu()
        } catch (e: NumberFormatException) {
            println("Please enter a number as the turnover")
            println()
        }
    }

}